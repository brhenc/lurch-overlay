# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="OMEMO for libpurple (Pidgin) using axolotl"
HOMEPAGE="https://github.com/gkdr/lurch"

GITHUB_REPO="${PN}"
GITHUB_USER="gkdr"
GITHUB_TAG="${PV}"

if [ ${PV} == "9999" ] ; then
    inherit git-r3

    EGIT_REPO_URI="https://github.com/${GITHUB_USER}/${PN}.git"
else
    SRC_URI="https://github.com/${GITHUB_USER}/${GITHUB_REPO}/tarball/v${GITHUB_TAG} -> ${PN}-${GITHUB_TAG}.tar.gz"
    KEYWORDS="*"
fi


LICENSE="GPL-3"
SLOT="0"
IUSE=""

DEPEND="dev-util/cmake
	dev-libs/libgcrypt
	dev-libs/mini-xml
	dev-db/sqlite"

RDEPEND="net-im/pidgin"

S="${WORKDIR}/${GITHUB_USER}-${GITHUB_REPO}-83048a5"
