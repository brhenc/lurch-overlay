# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Client lib for libsignal-protocol-c"
HOMEPAGE="https://github.com/gkdr/axc"

GITHUB_REPO="${PN}"
GITHUB_USER="gkdr"
GITHUB_TAG="${PV}"

if [ ${PV} == "9999" ] ; then
    inherit git-r3

    EGIT_REPO_URI="https://github.com/${GITHUB_USER}/${PN}.git"
else
    SRC_URI="https://github.com/${GITHUB_USER}/${GITHUB_REPO}/tarball/v${GITHUB_TAG} -> ${PN}-${GITHUB_TAG}.tar.gz"
    KEYWORDS="*"
fi

LICENSE="GPL-3"
SLOT="0"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

src_unpack() {
	if [ ${PV} == "9999" ] ; then
		git-r3_src_unpack
	else
		unpack ${A}
		mv "${WORKDIR}/${GITHUB_USER}-${PN}"-??????? "${S}" || die
	fi
	cd $S
	git submodule update --init
}
